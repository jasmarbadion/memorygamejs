//window.onload = myEventListener();
//function myEventListener(){
document.getElementById("playg").addEventListener("click", randomImages);
document.getElementById("endgame").addEventListener("click", endgame);

var topImg = [];
var mids = [];
var botts = [];
document.addEventListener("DOMContentLoaded"
, function(event)
{
init()
}
);

//This is to cache the top layer, middle layer and bottom layer images
function init(){
    topImg = document.getElementsByClassName('item');
    mids = ["images/baseball.jpg","images/basketball.jpg","images/football.jpg","images/gaming.jpg","images/hockey.jpg","images/soccer.jpg",
               "images/tennis.jpg","images/volleyball.jpg"];
    botts = ["images/airbud.jpg","images/hamsterboxing.jpg","images/hamstergolf.jpg","images/pigrugby.jpg","images/tigersoccer.jpg"];
}

var count = 0; //it is used to open and compare only two images when they are opened
var midElem; //contains the middle images that will be used to randomize inside the randomImages function
var checkIfStartClicked = 0; //it is used to alert user that they must press End Game first if they want to restart
var n = 0; //n is used for bottom images index
//this function is event handler for Start/Restart button
function randomImages(){
    //alert("works");
    if(numMatch != 8 && checkIfStartClicked > 0){
        alert("Must press End Game first to restart only if some parts of bottom image are shown");
    }
    checkIfStartClicked++;
    
    //addEventListeners are inside the randomImages() function so that user cannot
    //open any cover images without pressing the START/Restart button
    document.getElementById("lettera").addEventListener("click",openTopImgs);
    document.getElementById("letterb").addEventListener("click",openTopImgs);
    document.getElementById("letterc").addEventListener("click",openTopImgs);
    document.getElementById("letterd").addEventListener("click",openTopImgs);
    document.getElementById("lettere").addEventListener("click",openTopImgs);
    document.getElementById("letterf").addEventListener("click",openTopImgs);
    document.getElementById("letterg").addEventListener("click",openTopImgs);
    document.getElementById("letterh").addEventListener("click",openTopImgs);
    document.getElementById("letteri").addEventListener("click",openTopImgs);
    document.getElementById("letterj").addEventListener("click",openTopImgs);
    document.getElementById("letterk").addEventListener("click",openTopImgs);
    document.getElementById("letterl").addEventListener("click",openTopImgs);
    document.getElementById("letterm").addEventListener("click",openTopImgs);
    document.getElementById("lettern").addEventListener("click",openTopImgs);
    document.getElementById("lettero").addEventListener("click",openTopImgs);
    document.getElementById("letterp").addEventListener("click",openTopImgs);
    window.addEventListener("keyup",openTopImgs);
    
    var bottomImg = new Array();
    var middleImg = new Array();
    var botlayer;
    var cells;
    middleImg.push(mids[0]);
    middleImg.push(mids[1]);
    middleImg.push(mids[2]);
    middleImg.push(mids[3]);
    middleImg.push(mids[4]);
    middleImg.push(mids[5]);
    middleImg.push(mids[6]);
    middleImg.push(mids[7]);
    middleImg.push(mids[0]);
    middleImg.push(mids[1]);
    middleImg.push(mids[2]);
    middleImg.push(mids[3]);
    middleImg.push(mids[4]);
    middleImg.push(mids[5]);
    middleImg.push(mids[6]);
    middleImg.push(mids[7]);
    cells = document.getElementsByClassName('items');
    var i;
    midElem = new Array();
    for (i = 0; i < cells.length; i++){
        midElem[i] = imagesDifferent(middleImg);
        cells[i].innerHTML = "<img src='" + midElem[i] + "' />";
    }
    
    bottomImg.push(botts[0]);
    bottomImg.push(botts[1]);
    bottomImg.push(botts[2]);
    bottomImg.push(botts[3]);
    bottomImg.push(botts[4]);
    botlayer = document.getElementById('blayer');
    //n is the index of the bottom images so when it goes to last image the next game it will go to first image
    if (n == bottomImg.length){
        n = 0;
    }
    botlayer.innerHTML = "<img src='" + bottomImg[n] + "' />";
    n++;
}

//this function is to return random images to middle layer
function imagesDifferent(middleImg){
    var imgTemp = '';
    var num = Math.floor(Math.random()*middleImg.length);
    imgTemp = middleImg[num];
    middleImg.splice(num,1);
    return imgTemp;
}

    var first; //index of first opened cover
    var second; //index of second opened cover
//this function is event handler for the click events and keyup events of covers
function openTopImgs(e){ 
    var cid = this.id;
    var temp;
    //these ifs are for keyup event (Letters a-p)
    if (e.keyCode == 65){
        document.getElementById('lettera').click();
        }
    if (e.keyCode == 66){
        document.getElementById('letterb').click();
        }
    if (e.keyCode == 67){
        document.getElementById('letterc').click();
        }
    if (e.keyCode == 68){
        document.getElementById('letterd').click();
        }
    if (e.keyCode == 69){
        document.getElementById('lettere').click();
        }
    if (e.keyCode == 70){
        document.getElementById('letterf').click();
        }
    if (e.keyCode == 71){
        document.getElementById('letterg').click();
        }
    if (e.keyCode == 72){
        document.getElementById('letterh').click();
        }
    if (e.keyCode == 73){
        document.getElementById('letteri').click();
        }
    if (e.keyCode == 74){
        document.getElementById('letterj').click();
        }
    if (e.keyCode == 75){
        document.getElementById('letterk').click();
        }
    if (e.keyCode == 76){
        document.getElementById('letterl').click();
        }
    if (e.keyCode == 77){
        document.getElementById('letterm').click();
        }
    if (e.keyCode == 78){
        document.getElementById('lettern').click();
        }
    if (e.keyCode == 79){
        document.getElementById('lettero').click();
        }
    if (e.keyCode == 80){
        document.getElementById('letterp').click();
        }
    for (var i = 0; i < topImg.length; i++){
        if (topImg[i].id == cid){
            temp = i;
        }
    } 
    if (count == 0){
        openCovers(cid);
        first = temp;
        count = 1;
}
    else if(count == 1){
        openCovers(cid);
        second = temp;
        count = 0;
        //I do removeEventListener so that when two images are opened and it
        //is trying to compare them, user won't be able to click or open other images
        document.getElementById("lettera").removeEventListener("click",openTopImgs);
        document.getElementById("letterb").removeEventListener("click",openTopImgs);
        document.getElementById("letterc").removeEventListener("click",openTopImgs);
        document.getElementById("letterd").removeEventListener("click",openTopImgs);
        document.getElementById("lettere").removeEventListener("click",openTopImgs);
        document.getElementById("letterf").removeEventListener("click",openTopImgs);
        document.getElementById("letterg").removeEventListener("click",openTopImgs);
        document.getElementById("letterh").removeEventListener("click",openTopImgs);
        document.getElementById("letteri").removeEventListener("click",openTopImgs);
        document.getElementById("letterj").removeEventListener("click",openTopImgs);
        document.getElementById("letterk").removeEventListener("click",openTopImgs);
        document.getElementById("letterl").removeEventListener("click",openTopImgs);
        document.getElementById("letterm").removeEventListener("click",openTopImgs);
        document.getElementById("lettern").removeEventListener("click",openTopImgs);
        document.getElementById("lettero").removeEventListener("click",openTopImgs);
        document.getElementById("letterp").removeEventListener("click",openTopImgs);
        window.removeEventListener("keyup",openTopImgs);
        setTimeout("matchImg(first,second)",2000);
            }
}

//this function is to open cover images that are clicked
function openCovers(cid){
    var img = document.getElementById(cid);
    img.style.visibility = 'hidden';
}

var numMatch = 0; 
var attempts = 0;
//this function is to compare the images that were opened
function matchImg(first,second){
    //alert("function called" + first + "" + second);
    attempts++;
    var coverCells = topImg;
    var cells = document.getElementsByClassName('items');
    if (midElem[first] == midElem[second]){
        //alert("they match");
        numMatch++;
        cells[first].style.visibility = 'hidden';
        cells[second].style.visibility = 'hidden';
    }
    else{
        coverCells[first].style.visibility = 'visible';
        coverCells[second].style.visibility = 'visible';
    }
    //I add back the addEventListener when it was removed 
    document.getElementById("lettera").addEventListener("click",openTopImgs);
    document.getElementById("letterb").addEventListener("click",openTopImgs);
    document.getElementById("letterc").addEventListener("click",openTopImgs);
    document.getElementById("letterd").addEventListener("click",openTopImgs);
    document.getElementById("lettere").addEventListener("click",openTopImgs);
    document.getElementById("letterf").addEventListener("click",openTopImgs);
    document.getElementById("letterg").addEventListener("click",openTopImgs);
    document.getElementById("letterh").addEventListener("click",openTopImgs);
    document.getElementById("letteri").addEventListener("click",openTopImgs);
    document.getElementById("letterj").addEventListener("click",openTopImgs);
    document.getElementById("letterk").addEventListener("click",openTopImgs);
    document.getElementById("letterl").addEventListener("click",openTopImgs);
    document.getElementById("letterm").addEventListener("click",openTopImgs);
    document.getElementById("lettern").addEventListener("click",openTopImgs);
    document.getElementById("lettero").addEventListener("click",openTopImgs);
    document.getElementById("letterp").addEventListener("click",openTopImgs);
    window.addEventListener("keyup",openTopImgs);
    
    if (numMatch == 8){
        alert("Game completed! Number of attempts: " + attempts);
        setTimeout("endgame()",3000);
    }
}

//this function is event handler for End Game button
function endgame(){
    location.reload();
}
//}

